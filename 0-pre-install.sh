#!/bin/bash

# Pre-install to setup for the rest of the install scripts

# Connect to WLAN if not LAN
# iwctl --passphrase [password] station wlan0 connect amoebe

# Verify connect
# ping -c2 www.archlinux.org

# List drives
# lsblk

# Create partitions
# gdisk /dev/nvme0n1
# p1: +512M ef00
# p2: rest  8300
# write w, confirm y

# Proceed with 1-install.sh
